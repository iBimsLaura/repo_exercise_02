using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{


    public class Test : MonoBehaviour
    {
        private bool isEnemyAlive = true;
        private int hitpoints = 100;
        private float critChance = 10.5f;
        private string message = "Hallo";
        private bool grounded = false;

        public int stamina = 200;
        public bool isEnemyWounded = false;
        public string enemyName = "Herbert";
        public float speed = 50f;
        public bool isEnemyFrozen = false;

        public void Start()
        {
            Debug.Log(isEnemyAlive);
            Debug.Log(hitpoints);
            Debug.Log(critChance);
            Debug.Log(message);
            Debug.Log(grounded);
            Debug.Log(stamina);
            Debug.Log(isEnemyWounded);
            Debug.Log(enemyName);
            Debug.Log(speed);
            Debug.Log(isEnemyFrozen);
        }


    }



}

^1 TEST

using UnityEngine;
using UnityEditor;

namespace CoAHomework
{
    public class Player : MonoBehaviour
    {
        public int poisonDamage = 30;
        public string playerName = "Ursula";
        public float deflectChance = 30.5f;
        public bool isPlayerAttacking = false;
        public bool isPlayerDucking = true;

        public void Start()
        {
            Debug.Log(poisonDamage);
            Debug.Log(playerName);
            Debug.Log(deflectChance);
            Debug.Log(isPlayerAttacking);
            Debug.Log(isPlayerDucking);
        }
    }
}

2^ PLAYER

namespace CoAHomework
{
    public class Dog : MonoBehaviour
    {
        private int hunger;
        private bool isDogDistracted = true;
        private string message = "Woof";

        public void Awake()
        {
            Fressnapf(1);
        }

        public void Start()
        {


            Knochen(1, 2, 3);

            
        }

        public void Update()
        {





        }

        public void Knochen(int a, int b, int c)
        {

            return;

        }

        public void Fressnapf(int d)
        {

        }

    }
}

^3 DOG